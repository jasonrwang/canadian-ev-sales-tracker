# Canadian EV Sales Tracker

![](outputs/chartZEV.png)

## Description

Reads quarterly motor vehicle registration data from Statistics Canada and visualizes it in a pretty way.

## To Do

- [x] Automate deployment to GitLab Pages or elsewhere
- [ ] Automate data updating and re-running deployment if new data exists
- [ ] Enable Binder access with a badge

### Dashboard

- [ ] Find EV to charger data for Canada?
  - Electric Autonomy? PlugShare open data?
- [ ] Enhance viz
  - [x] Add hover with numbers
  - [ ] Label the bottom chart noting interval selection
- [ ] Include EV Sales Availability Targets
- [ ] Include forecasts
- [ ] Fix autoresponsive width issue
- [ ] ~~Make legend obviously interactive~~
- [ ] Determine the last data update date – may need to scrape StatCan site and verify
- [x] Set custom font (esp. for the interactive options)

## Data Sources

- [Statistics Canada, Table 20-10-0024-01 New motor vehicle registrations, quarterly](https://doi.org/10.25318/2010002401-eng)
