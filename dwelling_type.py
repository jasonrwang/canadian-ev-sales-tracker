# # Canadian MURB Household Share
# To determine how many households are in MURBs

# %%
from stats_can import StatsCan
import pandas as pd
import altair as alt
sc = StatsCan()


# %%
# ## Load and Clean Data
df = sc.table_to_df('9810013801')

# %% [markdown]
# ## Data Analysis

# %%
# Key Parameters
keyMunis = [
    'Alberta',
    'Calgary (CMA), Alta.',
    'Edmonton (CMA), Alta.',
    'Grande Prairie (CA), Alta.',
    'Red Deer (CMA), Alta.',
    'Lethbridge (CMA), Alta.',
    'Medicine Hat (CA), Alta.',
]

# %%
# Only keep the most recent census year
df = df.query("`Census year (3)`=='2021'")
# Only keep the column of all household types
df = df[['GEO','Structural type of dwelling (9)','Household type including multigenerational households (9):Total - Household type including multigenerational households[1]']]
df.columns = ['GEO','Type','Count']
df['Count'] = df['Count'].astype(int)

# %%
# Get a df of just totals
dfTotals = df.query("Type=='Total - Structural type of dwelling'")
dfTotals = dfTotals.set_index('GEO')['Count']

# %%
# Tag the MURBs
# https://www12.statcan.gc.ca/census-recensement/2021/ref/98-500/001/98-500-x2021001-eng.cfm
MURBtypes = [
    'Row house',
    'Apartment or flat in a duplex',
    'Apartment in a building that has fewer than five storeys',
    'Apartment in a building that has five or more storeys'
]
df['isMURB'] = df['Type'].isin(MURBtypes)

# # MURB percent of all dwelling types
dfGrouped = df.query('isMURB==True').groupby(['GEO'])['Count'].sum()
dfProportion = dfGrouped / dfTotals
dfProportion[keyMunis]


# %%
dfSummary = pd.concat([dfGrouped,dfTotals,dfProportion],axis=1)
dfSummary.columns = ['MURB Households','Total Households','Percent MURBs']
# %%
# ## Export data
dfSummary.loc[keyMunis].to_excel('outputs/MURBhouseholds.xlsx')
