# %% [markdown]
# # Adjust HTML
# Read the saved HTML content
with open('public/unformatted.html', 'r') as file:
    html_content = file.read()

custom_css = """
<!-- Load Noto Sans font -->
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Noto+Sans:wght@100..900&display=swap" rel="stylesheet">

<style>

/* Each group of selections */
.vega-bind {
    font-family: Noto Sans, Arial, sans-serif;
    display: block;
}

/* Selection category name */
.vega-bind-name {
    font-size: 14px;
    font-weight: bold;
    margin-right: 5px;
}

/* Dropdown and radio buttons labels */
select, input[type='radio'] {
    font-size: 12px;
}

/* Radio buttons labels */
label {
    font-size: 14px;
    margin-right: 5px;
}

/* Make responsive formatting more structured */
.vega-bindings .vega-bind { 
  display: flex;
  gap: 5px;
}

.vega-bindings .vega-bind .vega-bind-name {
  shrink: 0;
  width: 100px;
  display: inline-block;
}

.vega-bindings .vega-bind .vega-bind-radio label {
  text-wrap: nowrap;
}

input[type='radio'] {
  margin-left: 0;
}
</style>
"""

# Inject the custom CSS into the HTML content
html_with_css = html_content.replace('<head>', f'<head>{custom_css}')

# Save the modified HTML with injected CSS
with open('public/index.html', 'w') as f:
    f.write(html_with_css)

print("HTML with custom CSS exported as 'index.html'.")


# %%
