# %% [markdown]
# # Canadian EV Sales Tracker
# This code pulls EV sales data from StatsCan, fills in some gaps, and finally exports a visualization for web.

# %%
from stats_can import StatsCan
import pandas as pd
import altair as alt
sc = StatsCan()

# %% [markdown]
# ## Load and Clean Data

# %%
# Try to update locally cached table
def update(i=0):
    if i>2:
        raise RuntimeWarning('Attempted to update data three times without success probably due to a ConnectionError.')

    try:
        sc.update_tables()
    except FileNotFoundError:
        # Ignore this error if a cache doesn't exist
        pass
    except ConnectionError:
        update(i+1)

    # Load the EV sales table
    try:
        return sc.table_to_df('20-10-0024-01')
    except ConnectionError:
        update(i+1)
    except Exception as e:
        print(e)

    # If there is a data issue, exit unsuccessfully.
    print('Data update error.')
    exit(1)

df = update()

# %% [markdown]
# ## Data Analysis

# %% [markdown]
# ### Missing Regions
# AB, NS, and NFL don't have data. Since the total numbers for Canada are larger than the sum of all reported provinces, we assume that the rest of the provinces make up the remainder.

# %%
# Find the regions without any data
otherRegions = df.query('VALUE.isna()')['GEO'].unique().tolist()
otherRegions.sort() # Sort this so Alberta is first :)
otherRegions = ' & '.join(str(i) for i in otherRegions)

# Calculate Alberta-NS-NFL by finding the gap between the sum of all provinces and Canada
df = df.assign(is_CANADA= lambda x: x['GEO']=='Canada')
dfAlberta = df.groupby(['is_CANADA','REF_DATE','Fuel type','Vehicle type',])['VALUE'].sum() # Sum for the "Canada" and not regions
dfAlberta = dfAlberta.unstack(level='is_CANADA') # Move the is_CANADA grouping into a wide format
dfAlberta = dfAlberta.assign(OtherRegions = lambda x: x[True] - x[False]) # Find the difference in the groups of is_CANADA vs. or not
dfAlberta = dfAlberta['OtherRegions'].reset_index() # Only keep the newly calculated column
dfAlberta = dfAlberta.rename({'OtherRegions': 'VALUE'},axis=1) # Rename that to VALUE (axis=1 means rename columns)
dfAlberta['GEO'] = otherRegions # Label this data as for Alberta-NS-NFL (the missing regions)

# %%
# Add the Alberta&others data to the main dataset
df = df.merge(dfAlberta,how='outer')
# Drop the regions without values
df = df.dropna(subset=['VALUE'])

# %% [markdown]
# ### Calculate sales as percent of all fuel types

# %%
# df = df.assign(is_ZEV= lambda x: x['Fuel type'].isin(['Battery electric', 'Plug-in hybrid electric']))
dfProportion = df.groupby(['GEO','REF_DATE','Fuel type','Vehicle type'])['VALUE'].sum()
dfProportion = dfProportion.reset_index()
dfProportion = dfProportion.groupby(['GEO','REF_DATE','Vehicle type','Fuel type'])['VALUE'].sum() / \
    dfProportion.query("`Fuel type` not in(['All fuel types'])").groupby(['GEO','REF_DATE','Vehicle type'])['VALUE'].sum()
dfProportion = dfProportion.reset_index()
dfProportion = dfProportion.assign(is_ZEV = lambda x: x['Fuel type'].isin(['Battery electric', 'Plug-in hybrid electric'])) # .isin() returns True or False
dfProportion = dfProportion.rename({'VALUE':'percent'},axis=1)
dfProportion = dfProportion.dropna(subset=['percent'])

# %% [markdown]
# #### Compile the sum for ZEVs (both BEVs and PHEVs)

# %%
ZEV_label = 'ZEVs Total'

# %%
dfZEV = dfProportion.query("is_ZEV==True")
dfZEV = dfZEV.groupby(['GEO','REF_DATE','Vehicle type']).sum()['percent']
dfZEV = dfZEV.reset_index()
dfZEV['Fuel type'] = ZEV_label
dfZEV = pd.concat([dfProportion.query("is_ZEV==True"),dfZEV])

# %% [markdown]
# ### Export data

# %%
df.to_excel('outputs/all_data.xlsx')
dfProportion.to_excel('outputs/proportional_data.xlsx')
dfZEV.to_excel('outputs/ZEV.xlsx')


# %% [markdown]
# ## Visualizations

# %% [markdown]
# ### Parameters

# %%
# Find when the data was last updated --> right now, just latest data
latestDate = df['REF_DATE'].max()
# Format it as "QX YYYY"
latestDate = f'{latestDate.year} Q{latestDate.quarter}'

# %%
# Create a list of all the regions with names
regions = df['GEO'].unique().tolist() # (this line can be run earlier to verify which regions actually have data)
regions.remove('Canada')
regions.insert(0, 'Canada')

# Manually define the order in which each fuel type is sorted in the visualization
fuelOrder = "{\
    'Battery electric': 0,\
    'Plug-in hybrid electric': 1, \
    'Hybrid electric': 2, \
    'Gasoline': 3, \
    'Diesel': 4, \
    'Other fuel types': 5\
}[datum['Fuel type']]"

# Manually define the order for vehicle classes so Total shows up first
classOrder = df['Vehicle type'].unique().tolist()
classOrder.remove('Total, vehicle type')
classOrder.insert(0,'Total, vehicle type')

# %%
class_dropdown = alt.binding_radio(
    options = classOrder,
    name="Vehicle Class")
class_select = alt.selection_point(bind=class_dropdown, fields=['Vehicle type'] ,value='Total, vehicle type')

# %%
# Select year
interval = alt.selection_interval(encodings=['x'])
# A dropdown filter
region_dropdown = alt.binding_select(options=regions, name="Regions")
region_select = alt.selection_point(bind=region_dropdown, fields=['GEO'] ,value='Canada') # Default to Canada
fuel_select = alt.selection_point(fields=['Fuel type'],bind='legend') # Allow clicking on the legend – ideal to have hover cursor

# %% [markdown]
# ### Main Interactive for Export

# %%
# Percent ZEV by fuel type
chartZEVs = alt.Chart(dfZEV).transform_filter(
    class_select
).transform_filter(
    region_select
)

range_ = ['#669EBD','#005D8E','#AD1F23']

area = chartZEVs.transform_filter(
    alt.datum['Fuel type']!=ZEV_label
).mark_area().encode(
    x=alt.X('yearquarter(REF_DATE):T').title('Date'),
    y=alt.Y('sum(percent):Q').title('Percent of Vehicle Sales').axis(format='.1%'),
    color=alt.Color('Fuel type:N',sort=['Plug-in hybrid electric','Battery electric']).scale(range=range_).title('ZEV Type'),
    order=alt.Order('Fuel type:N', sort='ascending').title('ZEV Type')
).properties(
    title=alt.Title(
        'Electric Vehicle (EV) Proportion of Total Vehicle Sales by Province',
        subtitle=["This visualization updates automatically each quarter based on data from","Statistics Canada, Table 20-10-0024-01: New vehicle registrations, quarterly.",f"Data includes up to {latestDate}."],
        orient='bottom',
        anchor='start',
        frame='group',
        offset=20
    ),
    # width=800,
    width='container'
).add_params(
    region_select,
    class_select
).interactive()

# Selection that follows mouse
nearest = alt.selection_point(
    nearest=True,
    on='pointerover',
    clear='pointeroff',
    fields=['REF_DATE','Fuel type'],
    empty=False
)

#Draw a rule at the location of the selection
rules = chartZEVs.transform_pivot(
    'Fuel type',
    value='percent',
    groupby=['REF_DATE','GEO','Vehicle type']
).mark_rule(color='gray').encode(
    x='REF_DATE:T',
    opacity=alt.condition(nearest, alt.value(0.5), alt.value(0)),
    tooltip=[alt.Tooltip('yearquarter(REF_DATE):T').title('Date')] + \
        [alt.Tooltip(c, type='quantitative', format='.1%') for c in dfZEV['Fuel type'].unique()],
).add_params(
    nearest
)

# %%

mainChart = (area + rules).configure_legend(
    strokeColor='gray',
    fillColor='#EEEEEE',
    padding=10,
    cornerRadius=10,
    orient='top-left'
).configure_axis(
    grid=False,
    labelColor='#005D8E',
    titleColor='#005D8E'
)
mainChart

# %%
### Save Interactive to HTML and PNG
mainChart.properties(
    title='Canadian New Electric Vehicle Sales Proportions',
    width=400
).save('outputs/chartZEV.png',scale_factor=4)
mainChart.save('public/unformatted.html')

# %% [markdown]
# ## Other Analysis and Visualizations

# %% [markdown]
# Total Numbers

# %%
# Canada
dfProportion.query("GEO=='Canada' & `Vehicle type`=='Total, vehicle type' & is_ZEV").groupby(dfProportion['REF_DATE'].dt.year)['percent'].sum()/4 # Divide by 4 since it's summing four? quarters

# %%
# AB NFL NS by year
dfProportion.query("GEO=='Alberta & Newfoundland and Labrador & Nova Scotia' & `Vehicle type`=='Total, vehicle type' & is_ZEV").groupby(dfProportion['REF_DATE'].dt.year)['percent'].sum()/4

# %%
# AB NFL NS by quarter
df.query("GEO=='Alberta & Newfoundland and Labrador & Nova Scotia' & `Vehicle type`=='Total, vehicle type' & `Fuel type`.isin(['Battery electric','Plug-in hybrid electric'])").groupby(['REF_DATE'])['VALUE'].sum()

# %%
# All vehicle types sales (normalized)
base = alt.Chart(
    dfZEV.query("`Vehicle type`=='Total, vehicle type'")
).add_params(
    region_select
).transform_filter(
    region_select
)

normalized = base.mark_area().encode(
    x=alt.X('REF_DATE:T').title('Year'),
    y=alt.Y('sum(percent):Q').title('Percent of Total Vehicle Sales').axis(format='%'),
    color=alt.Color('Fuel type:N'), # Colour by the defined order
    order=alt.Order('order:O') # Use the defined order
)

chart = normalized.encode(
    x=alt.X('REF_DATE:T', scale=alt.Scale(domain=interval)).title('Year'),
    opacity=alt.condition(fuel_select, alt.value(1), alt.value(0.1)),
).add_params(
    fuel_select,
).properties(
    # width=800,
    width='container',
    height=300,
    title='Canadian Vehicle Sales Proportions by Fuel Type (Normalized)'
)

view = normalized.add_params(
    interval
).properties(
    # width=800,
    width='container',
    height=50,
)

chart & view

# %%
# ### Save Interactive to HTML and PNG
# normalized.properties(
#     title='Canadian Vehicle Sales Proportions by Fuel Type (Normalized)'
# ).save('outputs/chart.png',scale_factor=2)
# (chart & view).configure(autosize="fit-x").save('public/index.html')

# %%
# Percent ZEV
alt.Chart(dfZEV.query("is_ZEV==True")).mark_line().encode(
    x=alt.X('REF_DATE:T').title('Year'),
    y=alt.Y('sum(percent):Q').title(None).axis(format='%'),
    color=alt.Color('GEO').title('Region')
).properties(
    title='EV Proportion of Total Vehicle Sales by Province'
).add_params(
    class_select
).transform_filter(
    class_select
)

# %%
# Percent ZEV faceted by vehicle class
alt.Chart(dfZEV.query("`Vehicle type`!='Total, vehicle type'")).mark_line().encode(
    x=alt.X('REF_DATE:T').title('Year'),
    y=alt.Y('sum(percent):Q').title(None).axis(format='%'),
    color=alt.Color('GEO').title('Region'),
    facet='Vehicle type:N'
).properties(
    title='ZEV Proportion of Total Vehicle Sales by Province'
)
# %%
# Total vehicle sales volume
alt.Chart(df.query("`Fuel type`=='All fuel types'")).mark_line().encode(
    x=alt.X('REF_DATE:T').title('Year'),
    y=alt.Y('VALUE:Q').title('Cars Sold'),
    color=alt.Color('GEO').title('Region'),
    facet='Vehicle type:N'
).properties(
    title='Vehicle Sales by Province'
)
# Could split this into PHEV vs. BEV if of interest

# %%
# Total vehicle sales volume
alt.Chart(df.query("GEO!='Canada' & `Fuel type`=='All fuel types'")).mark_area().encode(
    x=alt.X('REF_DATE:T').title('Year'),
    y=alt.Y('sum(VALUE):Q').stack('normalize').title('Cars Sold'),
    color='Vehicle type:N',
    facet='GEO'
    # color=alt.Color('GEO').title('Region'),
    # facet='Vehicle type:N'
).properties(
    title='Vehicle Sales by Province'
)
# Could split this into PHEV vs. BEV if of interest

# %%
# Total vehicle sales by vehicle type
alt.Chart(df.query("GEO=='Canada'").query("`Fuel type`=='All fuel types'")).mark_line().encode(
    x=alt.X('REF_DATE:T').title('Year'),
    y=alt.Y('sum(VALUE):Q').title('Cars Sold'),
    color=alt.Color('Vehicle type:N').title('Vehicle Class'),
    facet='Fuel type:N'
).properties(
    title='Vehicle Sales by Class in Canada'
)
# %%
# ZEV Sales in Alberta with Labels
alt.Chart(
    dfZEV.query("GEO=='Alberta & Newfoundland and Labrador & Nova Scotia'")
).mark_area().encode(
    x=alt.X('REF_DATE:T').title('Year'),
    y=alt.Y('percent:Q').title('Percent of Total Vehicle Sales').axis(format='%'),
    color=alt.Color('Fuel type:N', sort=alt.SortField('order', 'descending')),
    order=alt.Order('order:O'),
    facet=alt.Facet('Vehicle type:N').title('Vehicle Class')
).properties(
    title='ZEV Sales as Proportion of Total Vehicle Sales in Alberta'
).transform_calculate(
    order=fuelOrder
)
# %%
dfZEV.query("GEO=='Alberta & Newfoundland and Labrador & Nova Scotia' & \
            REF_DATE=='2023-10-01'").groupby(['REF_DATE','Vehicle type','Fuel type'])['percent'].sum()
# %%
alt.Chart(
    df.query("GEO=='British Columbia and the Territories' & `Fuel type`=='Battery electric' & `Vehicle type`=='Total, vehicle type'")
).mark_line().encode(
    x='REF_DATE:T',
    y='VALUE:Q'
)

# %%
# If everything is successful, exit as such.
exit(0)