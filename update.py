# %% [markdown]
# # Canadian EV Sales Tracker
# This looks for updated data from StatsCan so a cronjob can decide when to run the rest of the data analysis.

# %%
from stats_can import StatsCan
sc = StatsCan()

# %% [markdown]
# ## Look for an update

# %%
# Try to update locally cached table
def update(i=0):

    if i>2:
        raise RuntimeWarning('Attempted to update data three times without success probably due to a ConnectionError.')

    try:
        if len(sc.update_tables())>1:
            print("Data updated.")
            exit(0)  # Return 0 to indicate success
    except FileNotFoundError:
        # Ignore this error if a cache doesn't exist
        print('No locally cached data.')
        pass
    except ConnectionError:
        update(i+1)
    
    print("No update.")
    exit(1)  # Return 1 to indicate no update

update()
